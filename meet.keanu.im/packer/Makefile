include ../Makefile.env
.PHONY: build all dev-debian-base
export AWS_SOURCE_REGION ?= $(AWS_REGION)
PACKER_BASE ?= ../../packer
ANSIBLE_BASE ?= ../../ansible
PACKER_ARGS ?=
export ANSIBLE_VARS_PLUGINS=$(ANSIBLE_BASE)/playbooks/plugins/vars

deps:
	cd $(ANSIBLE_BASE) && $(MAKE) deps

jvb: deps templates
	$(AWS_WRAP) "packer build $(PACKER_ARGS) -timestamp-ui -parallel=true -only='$(shell cat .targets-jvb)' $(PWD)/packer.json"

debian-base: deps templates
	$(AWS_WRAP) "packer build $(PACKER_ARGS) -timestamp-ui -parallel=true -only='$(shell cat .targets-debian-base)' $(PWD)/packer.json"

region/%: deps templates
	$(AWS_WRAP) "packer build $(PACKER_ARGS) -timestamp-ui -parallel=false -only='debian-base-$(@F),jvb-$(@F)' $(PWD)/packer.json"

jvb/%: deps templates
	$(AWS_WRAP) "packer build $(PACKER_ARGS) -timestamp-ui -parallel=true -only='jvb-$(@F)' $(PWD)/packer.json"

debian-base/%: deps templates
	$(AWS_WRAP) "packer build $(PACKER_ARGS) -timestamp-ui -parallel=true -only='debian-base-$(@F)' $(PWD)/packer.json"

templates: packer.json .targets-jvb .targets-debian-base inventory/group_vars/packer.yml

.PHONY: packer.json
packer.json:
	@gomplate -f $(PACKER_BASE)/templates/packer.json.tmpl -d config.yaml -o packer.json

.PHONY: .targets-jvb
.targets-jvb:
	@gomplate -f $(PACKER_BASE)/templates/targets-jvb.tmpl -d config.yaml -o .targets-jvb

.PHONY: .targets-debian-base
.targets-debian-base:
	@gomplate -f $(PACKER_BASE)/templates/targets-debian-base.tmpl -d config.yaml -o .targets-debian-base

.PHONY: inventory/group_vars/packer.yml
inventory/group_vars/packer.yml:
	@gomplate -f $(PACKER_BASE)/templates/ansible-vars-group-packer.yml.tmpl -d config.yaml -o inventory/group_vars/packer.yml

clean:
	@rm packer.json -f
	@rm .targets-* -f
	@rm inventory/group_vars/packer.yml

dist-clean: clean
	@rm manifest.json

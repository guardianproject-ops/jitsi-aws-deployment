data "aws_ami" "debian_base_eu" {
  most_recent = true
  owners      = ["self"]
  filter {
    name   = "tag:Application"
    values = ["debian-base"]
  }
  filter {
    name   = "tag:Namespace"
    values = [local.namespace]
  }
  filter {
    name   = "tag:Stage"
    values = [local.stage]
  }
}

module "jitsi_master_eu" {
  source                 = "../../../terraform-modules/jitsi-master-instance"
  namespace              = local.namespace
  stage                  = local.stage
  name                   = "master"
  tags                   = local.tags
  attributes             = [local.aws_region]
  region                 = local.aws_region
  playbook_bundle_s3_key = data.terraform_remote_state.setup.outputs.ansible_playbooks_bundle_s3_object.key
  playbook_bucket        = data.terraform_remote_state.setup.outputs.ssm_playbook.playbooks_bucket_id
  ssm_logs_bucket        = data.terraform_remote_state.setup.outputs.ssm_playbook.ssm_logs_bucket
  vpc_id                 = data.terraform_remote_state.vpc_main.outputs.vpc.vpc_id
  jitsi_vpc_cidr_blocks = [
    data.terraform_remote_state.vpc_main.outputs.vpc.vpc_cidr_block,
    data.terraform_remote_state.vpc_satellites.outputs.vpc_india.vpc_cidr_block,
    data.terraform_remote_state.vpc_satellites.outputs.vpc_us1.vpc_cidr_block
  ]
  subnet_id                        = data.terraform_remote_state.vpc_main.outputs.public_subnets.named_subnet_ids["jitsi1"]
  ami                              = data.aws_ami.debian_base_eu.id
  instance_type                    = "t3.small"
  disable_api_termination          = false
  jitsi_fqdn                       = data.sops_file.secrets.data["jitsi_fqdn_eu"]
  jitsi_home_server                = data.sops_file.secrets.data["jitsi_home_server"]
  prosody_turncredentials_secret   = data.sops_file.secrets.data["prosody_turncredentials_secret"]
  jitsi_meet_videobridge_secret    = data.sops_file.secrets.data["jitsi_meet_videobridge_secret"]
  jitsi_meet_videobridge_password  = data.sops_file.secrets.data["jitsi_meet_videobridge_password"]
  jitsi_meet_jicofo_secret         = data.sops_file.secrets.data["jitsi_meet_jicofo_secret"]
  jitsi_meet_jicofo_password       = data.sops_file.secrets.data["jitsi_meet_jicofo_password"]
  ssl_key_pem                      = data.terraform_remote_state.letsencrypt.outputs.cert.private_key_pem
  ssl_cert_pem                     = data.terraform_remote_state.letsencrypt.outputs.cert_fullchain_pem
  maxmind_update_license_key       = data.sops_file.secrets.data["maxmind_update_license_key"]
  maxmind_update_account_id        = data.sops_file.secrets.data["maxmind_update_account_id"]
  jitsi_brand_footer_items_json    = jsonencode(yamldecode(data.sops_file.secrets.raw).jitsi_brand_footer_items)
  jitsi_brand_name                 = data.sops_file.secrets.data["jitsi_brand_name"]
  jitsi_brand_org                  = data.sops_file.secrets.data["jitsi_brand_org"]
  jitsi_brand_left_watermark_url   = data.sops_file.secrets.data["jitsi_brand_left_watermark_url"]
  jitsi_brand_url                  = data.sops_file.secrets.data["jitsi_brand_url"]
  jitsi_brand_support_url          = data.sops_file.secrets.data["jitsi_brand_support_url"]
  jitsi_meet_secure_domain_enabled = false
  # jitsi_meet_secure_domain_users_json = jsonencode(yamldecode(data.sops_file.secrets.raw).jitsi_meet_secure_domain_users)
}

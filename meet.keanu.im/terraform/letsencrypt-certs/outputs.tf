output "cert" {
  value = acme_certificate.production
}

output "cert_fullchain_pem" {
  value       = "${acme_certificate.production.certificate_pem}${acme_certificate.production.issuer_pem}"
  description = "The full certificate chain including the issued cert and the intermediate issuer cert."
}

terraform {
  required_version = ">= 0.12"
  # comment the following line out when bootstrapping
 backend "s3" {}
}

provider "aws" {
  region = local.aws_region
}

module "label" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace  = local.namespace
  stage      = local.stage
  name       = local.name
  attributes = var.attributes
  delimiter  = var.delimiter
  tags       = local.tags
}

resource "aws_s3_bucket" "default" {
  bucket        = module.label.id
  acl           = var.acl
  region        = local.aws_region
  force_destroy = var.force_destroy

  versioning {
    enabled    = true
    mfa_delete = var.mfa_delete
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = module.label.tags
}

resource "aws_dynamodb_table" "with_server_side_encryption" {
  name           = module.label.id
  read_capacity  = var.read_capacity
  write_capacity = var.write_capacity
  hash_key       = "LockID" # https://www.terraform.io/docs/backends/types/s3.html#dynamodb_table

  server_side_encryption {
    enabled = true
  }

  lifecycle {
    ignore_changes = [read_capacity, write_capacity]
  }

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = module.label.tags
}



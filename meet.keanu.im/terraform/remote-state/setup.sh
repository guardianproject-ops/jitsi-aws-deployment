#!/bin/bash
set -e
if [ ! -f "../_vars/secrets.sops.yml" ]; then
  echo "You must create _vars/secrets.sops.yml before proceeding!"
  exit 1
fi

if [ -z "${TF_VAR_namespace}" ] || [ -z "${TF_VAR_stage}" ] || [ -z "${TF_VAR_aws_region}" ]; then
  echo "You mustn't call this script directly. Use make. See the docs."
  exit 1
fi

disable_s3_backend() {
  echo "Disabling s3 backend"
  sed -Ei 's/^\s*(\s+backend\s+)/#\1/' main.tf
}

enable_s3_backend() {
  echo "Enabling s3 backend"
  sed -Ei 's/^\s*#(\s+backend\s+)/\1/' main.tf
}

disable_s3_backend

rm -rf .terraform
make whoami
make init-remote-state
make apply

enable_s3_backend

namespace=$TF_VAR_namespace
stage=$TF_VAR_stage
region=$TF_VAR_aws_region
id="$namespace-$stage-terraform-state"


echo "bucket = \"$id\"" > ./backend.tfvars
echo "dynamodb_table = \"$id\"" >> ./backend.tfvars
echo "region = \"$region\"" >> ./backend.tfvars

# reimport the state
make 'init -backend-config=./backend.tfvars -backend-config="key=remote-state/terraform.tfstate"'

# make backend config available to other terraform roots
mv ./backend.tfvars ../backend.tfvars

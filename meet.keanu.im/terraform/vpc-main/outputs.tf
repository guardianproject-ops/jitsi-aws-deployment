output "vpc" {
  value = module.vpc
}

output "public_subnets" {
  value = module.public_subnets
}

output "public_subnet_az" {
  value = local.subnet_az
}

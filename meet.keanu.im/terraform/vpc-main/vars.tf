provider "sops" {}

data "sops_file" "secrets" {
  source_file = "../_vars/secrets.sops.yml"
}

variable "aws_region" {
  type = string
}

locals {
  namespace  = var.namespace
  stage      = var.stage
  name       = "vpc-main"
  aws_region = var.aws_region
  tags       = map("Project", data.sops_file.secrets.data["project_tag"])

  cidr_block               = data.sops_file.secrets.data["vpc_main_cidr_block"]
  subnet_az                = data.sops_file.secrets.data["vpc_main_subnet_az"]
  victorops_cloudwatch_url = data.sops_file.secrets.data["victorops_cloudwatch_url"]
}

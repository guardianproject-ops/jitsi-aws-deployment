terraform {
  required_version = ">= 0.12"

  backend "s3" {
    key = "monitoring/terraform.tfstate"
  }
  required_providers {
    sops = "~> 0.5"
  }
}

provider "aws" {}

data "terraform_remote_state" "setup" {
  backend = "s3"

  config = {
    bucket = local.remote_state_bucket
    key    = "setup/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc_satellites" {
  backend = "s3"

  config = {
    bucket = local.remote_state_bucket
    key    = "vpc-satellites/terraform.tfstate"
  }
}

data "terraform_remote_state" "vpc_main" {
  backend = "s3"

  config = {
    bucket = local.remote_state_bucket
    key    = "vpc-main/terraform.tfstate"
  }
}

data "aws_ami" "debian_base" {
  most_recent = true
  owners      = ["self"]
  filter {
    name   = "tag:Application"
    values = ["debian-base"]
  }
  filter {
    name   = "tag:Namespace"
    values = [local.namespace]
  }
  filter {
    name   = "tag:Stage"
    values = [local.stage]
  }
}

module "monitoring_instance" {
  source                 = "../../../terraform-modules/monitoring"
  namespace              = local.namespace
  stage                  = local.stage
  name                   = local.name
  tags                   = local.tags
  attributes             = var.attributes
  playbook_bundle_s3_key = data.terraform_remote_state.setup.outputs.ansible_playbooks_bundle_s3_object.key
  playbook_bucket        = data.terraform_remote_state.setup.outputs.ssm_playbook.playbooks_bucket_id
  ssm_logs_bucket        = data.terraform_remote_state.setup.outputs.ssm_playbook.ssm_logs_bucket
  vpc_id                 = data.terraform_remote_state.vpc_main.outputs.vpc.vpc_id
  subnet_id              = data.terraform_remote_state.vpc_main.outputs.public_subnets.named_subnet_ids["jitsi1"]
  ami                    = data.aws_ami.debian_base.id
  disk_size_gb           = 15
  instance_type          = "t3.small"
  az                     = data.terraform_remote_state.vpc_main.outputs.public_subnet_az


  jitsi_fqdn                        = data.sops_file.secrets.data.jitsi_fqdn_eu
  cloudflare_origin_ca_key          = data.sops_file.secrets.data.cloudflare_origin_ca_key
  cloudflare_auth_key               = data.sops_file.secrets.data.cloudflare_auth_key
  cloudflare_auth_email             = data.sops_file.secrets.data.cloudflare_auth_email
  cloudflare_zone                   = data.sops_file.secrets.data.cloudflare_zone
  cloudflare_zone_id                = data.sops_file.secrets.data.cloudflare_zone_id
  monitoring_domain                 = data.sops_file.secrets.data.monitoring_domain
  matrix_alertmanager_shared_secret = data.sops_file.secrets.data.matrix_alertmanager_shared_secret
  matrix_alertmanager_url           = module.matrix_alertmanager.lambda_url
  alertmanager_receivers = replace(replace(data.sops_file.secrets.data.alertmanager_receivers, "matrix_alertmanager_shared_secret", data.sops_file.secrets.data.matrix_alertmanager_shared_secret),
  "matrix_alertmanager_url", module.matrix_alertmanager.lambda_url)
  alertmanager_route = yamldecode(data.sops_file.secrets.raw).alertmanager_route
}

module "matrix_alertmanager" {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-aws-lambda-matrix-alertmanager.git?ref=master"

  namespace  = local.namespace
  name       = local.name
  stage      = local.stage
  attributes = ["matrix-alertmanager"]
  tags       = local.tags

  matrix_alertmanager_shared_secret  = data.sops_file.secrets.data.matrix_alertmanager_shared_secret
  matrix_alertmanager_homeserver_url = data.sops_file.secrets.data.matrix_alertmanager_homeserver_url
  matrix_alertmanager_rooms = [
    data.sops_file.secrets.data.matrix_alertmanager_room,
  ]
  matrix_alertmanager_user_token = data.sops_file.secrets.data.matrix_alertmanager_user_token
  matrix_alertmanager_user_id    = data.sops_file.secrets.data.matrix_alertmanager_user_id
}

data "aws_ami" "jvb_eu" {
  provider    = aws
  most_recent = true
  owners      = ["self"]
  filter {
    name   = "tag:Application"
    values = ["jvb"]
  }
  filter {
    name   = "tag:Namespace"
    values = [local.namespace]
  }
  filter {
    name   = "tag:Stage"
    values = [local.stage]
  }
}

module "jitsi_jvb_eu" {
  source                  = "../../../terraform-modules/jitsi-videobridge"
  namespace               = local.namespace
  stage                   = local.stage
  name                    = "jvb"
  tags                    = local.tags
  attributes              = [local.aws_region]
  jitsi_master_region     = local.aws_region
  playbook_bucket         = data.terraform_remote_state.setup.outputs.ssm_playbook.playbooks_bucket_id
  ssm_logs_bucket         = data.terraform_remote_state.setup.outputs.ssm_playbook.ssm_logs_bucket
  vpc_id                  = data.terraform_remote_state.vpc_main.outputs.vpc.vpc_id
  subnet_id               = data.terraform_remote_state.vpc_main.outputs.public_subnets.named_subnet_ids["jitsi1"]
  jitsi_vpc_cidr_blocks   = local.jitsi_vpc_cidr_blocks
  ami                     = data.aws_ami.jvb_eu.id
  instance_type           = "t3.small"
  disable_api_termination = false # TODO change in prod
  common_ssm_prefix       = data.terraform_remote_state.jitsi_master.outputs.jitsi_master_eu.common_ssm_prefix
  ssm_kms_key_arn         = data.terraform_remote_state.jitsi_master.outputs.jitsi_master_eu.ssm_kms_key_arn
}

variable "sns_topic_subscription_https_endpoint" {
  type        = string
  description = "The HTTPS endpoint url to send cloudwatch SNS events to"
}

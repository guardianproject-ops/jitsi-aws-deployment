data "aws_region" "current" {}

module "label_cloudwatch" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace  = var.namespace
  stage      = var.stage
  name       = var.name
  attributes = [data.aws_region.current.name]
  tags       = var.tags
}

resource "aws_sns_topic" "cloudwatch_alarms" {
  name = module.label_cloudwatch.id
  tags = module.label_cloudwatch.tags
}

resource "aws_sns_topic_subscription" "cloudwatch_sns_subscription" {
  topic_arn              = aws_sns_topic.cloudwatch_alarms.arn
  protocol               = "https"
  endpoint               = var.sns_topic_subscription_https_endpoint
  endpoint_auto_confirms = true
}

#########################
# Cloudwatch Alarms     #
#########################

resource "aws_cloudwatch_metric_alarm" "failed_commands_alarm" {
  alarm_name        = "FailedAnsibleRuns"
  alarm_description = "The number of failed SSM RunCommands in the past 5 minutes"

  namespace   = "AWS/SSM-RunCommand"
  metric_name = "CommandsFailed"
  alarm_actions = [
    aws_sns_topic.cloudwatch_alarms.arn
  ]
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  datapoints_to_alarm = 1
  ok_actions          = []
  period              = 300
  statistic           = "Sum"
  threshold           = 1
  treat_missing_data  = "notBreaching"
  tags                = module.label_cloudwatch.tags
}


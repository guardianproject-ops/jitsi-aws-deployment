variable "jitsi_meet_secure_domain_enabled" {
  type        = bool
  default     = false
  description = "whether or not a password is required to host a meeting"
}
variable "jitsi_meet_secure_domain_users_json" {
  type        = string
  default     = "[]"
  description = "note this must be a string containing json"
}
variable "jitsi_brand_footer_items_json" {
  type        = string
  description = "note this must be a string containing json"
}
variable "jitsi_brand_name" {
  type = string
}
variable "jitsi_brand_org" {
  type = string
}
variable "jitsi_brand_left_watermark_url" {
  type = string
}
variable "jitsi_brand_url" {
  type = string
}
variable "jitsi_brand_support_url" {
  type = string
}
variable "maxmind_update_account_id" {
  type = string
}
variable "maxmind_update_license_key" {
  type = string
}
variable "ssl_key_pem" {
  type        = string
  description = "PEM encoded SSL private key"
}

variable "ssl_cert_pem" {
  type        = string
  description = "PEM encoded SSL certificate, it should be the full chain including any intermediate certs"
}

variable "jitsi_fqdn" {
  type = string
}
variable "jitsi_home_server" {
  type = string
}
variable "jitsi_meet_videobridge_secret" {
  type = string
}
variable "jitsi_meet_videobridge_password" {
  type = string
}
variable "jitsi_meet_jicofo_secret" {
  type = string
}
variable "jitsi_meet_jicofo_password" {
  type = string
}
variable "prosody_turncredentials_secret" {
  type = string
}

variable "vpc_id" {
  type        = string
  description = "the vpc id this asset is being deployed into"
}

variable "jitsi_vpc_cidr_blocks" {
  type        = list
  description = "list of cidr blocks that jitsi servers are deployed in, used in security group rules"
}

variable "region" {
  type        = string
  description = "AWS Region this session manager config is for"
}

variable "ami" {
  type        = string
  description = "AMI id for the instance"
}

variable "instance_type" {
  type        = string
  default     = "t3.nano"
  description = "ec2 instance type"
}

variable "subnet_id" {
  description = "the subnet id to place the instance on"
  type        = string
}

variable "playbook_bucket" {
  description = "the s3 bucket id that the instance can read ansible playbooks from"
  type        = string
}

variable "playbook_bundle_s3_key" {
  description = "the path to the ansible bundle zip file in the s3 bucket"
  type        = string
}

variable "ssm_logs_bucket" {
  type        = string
  description = "S3 bucket name of the bucket where SSM Session Manager logs are stored"
}

variable "disable_api_termination" {
  type        = bool
  default     = true
  description = "Set to false to enable terraform destroy/re-apply"
}

module "label" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace  = var.namespace
  stage      = var.stage
  name       = var.name
  delimiter  = var.delimiter
  attributes = var.attributes
  tags       = merge(var.tags, map("Application", local.tag_application, "JitsiMasterRegion", var.region))
}

locals {
  tag_application        = "jitsi-master"
  self_ssm_prefix        = module.ssm_prefix_self.full_prefix
  common_ssm_prefix      = module.ssm_prefix_common.full_prefix
  ansible_document_name  = "AWS-ApplyAnsiblePlaybooks"
  jitsi_association_name = "JitsiMasterApply"
}

###############################
# Jitsi Master private params #
###############################
resource "aws_ssm_parameter" "jitsi_meet_secure_domain_enabled" {
  name        = "${local.self_ssm_prefix}/jitsi_meet_secure_domain_enabled"
  description = ""
  type        = "String"
  value       = var.jitsi_meet_secure_domain_enabled ? "true" : "false"
  tags        = module.label.tags
}

resource "aws_ssm_parameter" "jitsi_meet_secure_domain_users_json" {
  name        = "${local.self_ssm_prefix}/jitsi_meet_secure_domain_users_json"
  description = "this must be json encoded as a string"
  type        = "String"
  value       = var.jitsi_meet_secure_domain_users_json
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "jitsi_brand_footer_items_json" {
  name        = "${local.self_ssm_prefix}/jitsi_brand_footer_items_json"
  description = "this must be json encoded as a string"
  type        = "String"
  value       = var.jitsi_brand_footer_items_json
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "jitsi_brand_name" {
  name        = "${local.self_ssm_prefix}/jitsi_brand_name"
  description = ""
  type        = "String"
  value       = var.jitsi_brand_name
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "jitsi_brand_org" {
  name        = "${local.self_ssm_prefix}/jitsi_brand_org"
  description = ""
  type        = "String"
  value       = var.jitsi_brand_org
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "jitsi_brand_left_watermark_url" {
  name        = "${local.self_ssm_prefix}/jitsi_brand_left_watermark_url"
  description = ""
  type        = "String"
  value       = var.jitsi_brand_left_watermark_url
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "jitsi_brand_url" {
  name        = "${local.self_ssm_prefix}/jitsi_brand_url"
  description = ""
  type        = "String"
  value       = var.jitsi_brand_url
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "jitsi_brand_support_url" {
  name        = "${local.self_ssm_prefix}/jitsi_brand_support_url"
  description = ""
  type        = "String"
  value       = var.jitsi_brand_support_url
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "nginx_ssl_crt" {
  name        = "${local.self_ssm_prefix}/ssl_cert"
  description = ""
  type        = "SecureString"
  value       = var.ssl_cert_pem
  tags        = module.label.tags
}

resource "aws_ssm_parameter" "nginx_ssl_key" {
  name        = "${local.self_ssm_prefix}/ssl_key"
  description = ""
  type        = "SecureString"
  value       = var.ssl_key_pem
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "prosody_turncredentials_secret" {
  name        = "${local.self_ssm_prefix}/prosody_turncredentials_secret"
  description = ""
  type        = "SecureString"
  value       = var.prosody_turncredentials_secret
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "jitsi_meet_jicofo_secret" {
  name        = "${local.self_ssm_prefix}/jitsi_meet_jicofo_secret"
  description = ""
  type        = "SecureString"
  value       = var.jitsi_meet_jicofo_secret
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "jitsi_meet_jicofo_password" {
  name        = "${local.self_ssm_prefix}/jitsi_meet_jicofo_password"
  description = ""
  type        = "SecureString"
  value       = var.jitsi_meet_jicofo_password
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "maxmind_update_license_key" {
  name        = "${local.self_ssm_prefix}/maxmind_update_license_key"
  description = ""
  type        = "SecureString"
  value       = var.maxmind_update_license_key
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "maxmind_update_account_id" {
  name        = "${local.self_ssm_prefix}/maxmind_update_account_id"
  description = ""
  type        = "SecureString"
  value       = var.maxmind_update_account_id
  tags        = module.label.tags
}
#######################
# Common params       #
#######################
resource "aws_ssm_parameter" "jitsi_fqdn" {
  name        = "${local.common_ssm_prefix}/jitsi_fqdn"
  description = ""
  type        = "String"
  value       = var.jitsi_fqdn
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "jitsi_home_server" {
  name        = "${local.common_ssm_prefix}/jitsi_home_server"
  description = ""
  type        = "String"
  value       = var.jitsi_home_server
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "jitsi_meet_videobridge_secret" {
  name        = "${local.common_ssm_prefix}/jitsi_meet_videobridge_secret"
  description = ""
  type        = "SecureString"
  value       = var.jitsi_meet_videobridge_secret
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "jitsi_meet_videobridge_password" {
  name        = "${local.common_ssm_prefix}/jitsi_meet_videobridge_password"
  description = ""
  type        = "SecureString"
  value       = var.jitsi_meet_videobridge_password
  tags        = module.label.tags
}
resource "aws_ssm_parameter" "public_ip" {
  name        = "${local.common_ssm_prefix}/public_ip"
  description = ""
  type        = "String"
  value       = aws_eip.elastic_ip.public_ip
  tags        = module.label.tags
  depends_on = [
    aws_instance.jitsi_master,
    aws_eip.elastic_ip
  ]
}
resource "aws_ssm_parameter" "private_ip" {
  name        = "${local.common_ssm_prefix}/private_ip"
  description = ""
  type        = "String"
  value       = aws_eip.elastic_ip.private_ip
  tags        = module.label.tags
  depends_on = [
    aws_instance.jitsi_master,
    aws_eip.elastic_ip
  ]
}

#######################
# IAM Resources       #
#######################

# create a kms_key to encrypt SSM parameters with
module "kms_key" {
  source     = "git::https://github.com/cloudposse/terraform-aws-kms-key.git?ref=tags/0.4.0"
  name       = var.name
  namespace  = var.namespace
  stage      = var.stage
  attributes = module.label.attributes
  tags       = var.tags

  description = "KMS key for ${module.label.id}"
  alias       = "alias/${module.label.id}"
}

# create a policy that allows the instance to use session manager and send logs an bucket
module "session_manager" {
  source         = "git::https://gitlab.com/guardianproject-ops/terraform-aws-session-manager-instance-policy?ref=master"
  name           = var.name
  namespace      = var.namespace
  stage          = var.stage
  attributes     = module.label.attributes
  tags           = var.tags
  s3_bucket_name = var.ssm_logs_bucket
  s3_key_prefix  = module.label.id
}

# create a policy to let the instance fetch its own prefix from SSM params
module "ssm_prefix_self" {
  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=tags/2.0.0"
  path_prefix       = "${module.label.id}/jitsi-master"
  prefix_with_label = false
  name              = var.name
  namespace         = var.namespace
  stage             = var.stage
  attributes        = module.label.attributes
  tags              = var.tags
  region            = var.region
  kms_key_arn       = module.kms_key.key_arn
}

# create a policy to let the instance fetch the common prefix from SSM params
module "ssm_prefix_common" {
  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=tags/2.0.0"
  path_prefix       = "${module.label.namespace}-${module.label.stage}-common/jitsi-master"
  prefix_with_label = false
  name              = var.name
  namespace         = var.namespace
  stage             = var.stage
  attributes        = concat(module.label.attributes, ["com"])
  tags              = var.tags
  region            = var.region
  kms_key_arn       = module.kms_key.key_arn
}

# define a policy that allows the instance to read from the ansible playbook bucket
data "aws_iam_policy_document" "playbook_bucket_access" {
  statement {
    sid = "AllowToReadFromPlaybookBucket"
    actions = [
      "s3:Get*",
      "s3:List*",
    ]

    resources = [
      "arn:aws:s3:::${var.playbook_bucket}",
      "arn:aws:s3:::${var.playbook_bucket}/*"
    ]
  }
}

resource "aws_iam_policy" "playbook_bucket_access" {
  name        = "playbook-bucket-access-${module.label.id}"
  description = "Policy that allows instances readonly access to playbook bucket"
  policy      = data.aws_iam_policy_document.playbook_bucket_access.json
}

# define a policy that allows the instance to write to the session manager logs bucket
data "aws_iam_policy_document" "session_manager_bucket_access" {
  statement {
    sid = "AllowWriteToSessionManagerBucket"
    actions = [
      "s3:Put*"
    ]

    resources = [
      "arn:aws:s3:::${var.ssm_logs_bucket}",
      "arn:aws:s3:::${var.ssm_logs_bucket}/*"
    ]
  }
}

resource "aws_iam_policy" "session_manager_bucket_access" {
  name        = "session-manager-bucket-access-${module.label.id}"
  description = "Policy that allows instances write access to the session manager logs bucket"
  policy      = data.aws_iam_policy_document.session_manager_bucket_access.json
}

# attach policies to the instance role
module "instance_role_attachment" {
  source     = "git::https://gitlab.com/guardianproject-ops/terraform-aws-iam-instance-role-policy-attachment?ref=master"
  name       = var.name
  namespace  = var.namespace
  stage      = var.stage
  attributes = module.label.attributes

  iam_policy_arns = [
    module.session_manager.ec2_session_manager_policy_arn,
    aws_iam_policy.playbook_bucket_access.arn,
    aws_iam_policy.session_manager_bucket_access.arn,
    module.ssm_prefix_self.policy_arn,
    module.ssm_prefix_common.policy_arn,
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  ]
}

# create the instance profile with the role
resource "aws_iam_instance_profile" "profile" {
  name = module.label.id
  role = module.instance_role_attachment.instance_role_id
}

#######################
# EC2 Resources       #
#######################

resource "aws_eip" "elastic_ip" {
  vpc      = true
  tags     = module.label.tags
  instance = aws_instance.jitsi_master.id
}

# create the ec2 instance itself
resource "aws_instance" "jitsi_master" {
  ami                     = var.ami
  instance_type           = var.instance_type
  subnet_id               = var.subnet_id
  disable_api_termination = var.disable_api_termination

  vpc_security_group_ids = [
    aws_security_group.jitsi_master.id,
  ]

  root_block_device {
    encrypted   = true
    volume_size = 20
  }

  iam_instance_profile = aws_iam_instance_profile.profile.id

  tags = module.label.tags
}

# create the security group for the instance
resource "aws_security_group" "jitsi_master" {
  name   = module.label.id
  tags   = module.label.tags
  vpc_id = var.vpc_id



  lifecycle {
    create_before_destroy = true
  }

  ingress {
    from_port   = 10000
    to_port     = 10000
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # node_exporter
  ingress {
    from_port   = 9100
    to_port     = 9100
    protocol    = "tcp"
    cidr_blocks = var.jitsi_vpc_cidr_blocks
  }
  # blackbox exporter
  ingress {
    from_port   = 9115
    to_port     = 9115
    protocol    = "tcp"
    cidr_blocks = var.jitsi_vpc_cidr_blocks
  }
  # jvb octo
  ingress {
    from_port   = 4096
    to_port     = 4096
    protocol    = "udp"
    cidr_blocks = var.jitsi_vpc_cidr_blocks
  }
  ingress {
    from_port   = 5222
    to_port     = 5222
    protocol    = "tcp"
    cidr_blocks = var.jitsi_vpc_cidr_blocks
  }

  ingress {
    from_port   = 5347
    to_port     = 5347
    protocol    = "tcp"
    cidr_blocks = var.jitsi_vpc_cidr_blocks
  }
  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_ssm_association" "jitsi_master" {
  name             = local.ansible_document_name
  association_name = local.jitsi_association_name
  targets {
    key    = "tag:Application"
    values = [local.tag_application]
  }
  output_location {
    s3_bucket_name = var.ssm_logs_bucket
    s3_key_prefix  = "${var.name}/${local.jitsi_association_name}"
  }
  parameters = {
    SourceType          = "S3"
    SourceInfo          = "{ \"path\": \"https://s3.amazonaws.com/${var.playbook_bucket}/${var.playbook_bundle_s3_key}\" }"
    InstallDependencies = "False"
    PlaybookFile        = "playbooks/jitsi-master.yml"
    ExtraVariables      = "SSM=True"
    Check               = "False"
    Verbose             = "-vvvv"
  }
}

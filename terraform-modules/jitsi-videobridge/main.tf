locals {
  ansible_document_name  = "AWS-ApplyAnsiblePlaybooks"
  jitsi_association_name = "JitsiVideoBridgeApply"
  tag_application        = "jvb"
}

module "label" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace  = var.namespace
  stage      = var.stage
  name       = var.name
  delimiter  = var.delimiter
  attributes = var.attributes
  tags       = merge(var.tags, map("Application", local.tag_application, "JitsiMasterRegion", var.jitsi_master_region))
}

module "ssm_prefix_common" {
  source            = "git::https://gitlab.com/guardianproject-ops/terraform-aws-ssm-param-store-iam?ref=tags/2.0.0"
  path_prefix       = var.common_ssm_prefix
  prefix_with_label = false
  name              = var.name
  namespace         = var.namespace
  stage             = var.stage
  attributes        = concat(module.label.attributes, ["common"])
  tags              = module.label.tags
  region            = var.jitsi_master_region
  kms_key_arn       = var.ssm_kms_key_arn
}

# create a policy that allows the instance to use session manager and send logs an bucket
module "session_manager" {
  source         = "git::https://gitlab.com/guardianproject-ops/terraform-aws-session-manager-instance-policy?ref=tags/0.2.0"
  name           = var.name
  namespace      = var.namespace
  stage          = var.stage
  attributes     = module.label.attributes
  tags           = module.label.tags
  s3_bucket_name = var.ssm_logs_bucket
  s3_key_prefix  = module.label.id
}

# define a policy that allows the instance to describe instance tags
data "aws_iam_policy_document" "describe_tags" {
  statement {
    actions = [
      "ec2:DescribeTags"
    ]

    resources = ["*"]
  }
}

resource "aws_iam_policy" "describe_tags" {
  name        = "describe-tags-${module.label.id}"
  description = "Policy that allows instances to describe instance tags"
  policy      = data.aws_iam_policy_document.describe_tags.json
}

# define a policy that allows the instance to read from the ansible playbook bucket
data "aws_iam_policy_document" "playbook_bucket_access" {
  statement {
    sid = "AllowToReadFromPlaybookBucket"
    actions = [
      "s3:Get*",
      "s3:List*",
    ]

    resources = [
      "arn:aws:s3:::${var.playbook_bucket}",
      "arn:aws:s3:::${var.playbook_bucket}/*"
    ]
  }
}

resource "aws_iam_policy" "playbook_bucket_access" {
  name        = "playbook-bucket-access-${module.label.id}"
  description = "Policy that allows instances readonly access to playbook bucket"
  policy      = data.aws_iam_policy_document.playbook_bucket_access.json
}

# define a policy that allows the instance to write to the session manager logs bucket
data "aws_iam_policy_document" "session_manager_bucket_access" {
  statement {
    sid = "AllowWriteToSessionManagerBucket"
    actions = [
      "s3:Put*"
    ]

    resources = [
      "arn:aws:s3:::${var.ssm_logs_bucket}",
      "arn:aws:s3:::${var.ssm_logs_bucket}/*"
    ]
  }
}

resource "aws_iam_policy" "session_manager_bucket_access" {
  name        = "session-manager-bucket-access-${module.label.id}"
  description = "Policy that allows instances write access to the session manager logs bucket"
  policy      = data.aws_iam_policy_document.session_manager_bucket_access.json
}

# attach policies to the instance role
module "instance_role_attachment" {
  source     = "git::https://gitlab.com/guardianproject-ops/terraform-aws-iam-instance-role-policy-attachment?ref=1.0.0"
  name       = var.name
  namespace  = var.namespace
  stage      = var.stage
  attributes = module.label.attributes

  iam_policy_arns = [
    module.session_manager.ec2_session_manager_policy_arn,
    aws_iam_policy.playbook_bucket_access.arn,
    aws_iam_policy.describe_tags.arn,
    aws_iam_policy.session_manager_bucket_access.arn,
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  ]
}

# create the instance profile with the role
resource "aws_iam_instance_profile" "profile" {
  name = module.label.id
  role = module.instance_role_attachment.instance_role_id
}

resource "aws_eip" "elastic_ip" {
  vpc      = true
  tags     = module.label.tags
  instance = aws_instance.jitsi_jvb.id
}

# create the ec2 instance itself
resource "aws_instance" "jitsi_jvb" {
  ami                     = var.ami
  instance_type           = var.instance_type
  subnet_id               = var.subnet_id
  disable_api_termination = var.disable_api_termination

  vpc_security_group_ids = [
    aws_security_group.jitsi_jvb.id,
  ]

  root_block_device {
    encrypted   = true
    volume_size = 25
  }

  iam_instance_profile = aws_iam_instance_profile.profile.id

  tags = module.label.tags
}

# create the security group for the instance
resource "aws_security_group" "jitsi_jvb" {
  name   = module.label.id
  tags   = module.label.tags
  vpc_id = var.vpc_id

  lifecycle {
    create_before_destroy = true
  }

  ingress {
    from_port   = 10000
    to_port     = 10000
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # node_exporter
  ingress {
    from_port   = 9100
    to_port     = 9100
    protocol    = "tcp"
    cidr_blocks = var.jitsi_vpc_cidr_blocks
  }
  # jitsi exporter
  ingress {
    from_port   = 9700
    to_port     = 9700
    protocol    = "tcp"
    cidr_blocks = var.jitsi_vpc_cidr_blocks
  }
  # jvb octo
  ingress {
    from_port   = 4096
    to_port     = 4096
    protocol    = "udp"
    cidr_blocks = var.jitsi_vpc_cidr_blocks
  }
  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_ssm_association" "jitsi_jvb" {
  name             = local.ansible_document_name
  association_name = local.jitsi_association_name

  targets {
    key    = "tag:Application"
    values = [local.tag_application]
  }
  output_location {
    s3_bucket_name = var.ssm_logs_bucket
    s3_key_prefix  = "${var.name}/${local.jitsi_association_name}"
  }
  parameters = {
    SourceType = "S3"

    SourceInfo          = "{ \"path\": \"https://s3.amazonaws.com/${var.playbook_bucket}/${var.playbook_bundle_s3_key}\" }"
    InstallDependencies = "False"
    PlaybookFile        = "playbooks/jitsi-videobridge.yml"
    ExtraVariables      = "SSM=True"
    Check               = "False"
    Verbose             = "-vvvv"
  }
}

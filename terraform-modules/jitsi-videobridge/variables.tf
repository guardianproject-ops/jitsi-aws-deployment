variable "common_ssm_prefix" {
  type        = string
  description = "the ssm prefix shared among the jitsi master and jvbs"
  #validation {
  #  condition     = length(var.common_ssm_prefix) > 1 && substr(var.common_ssm_prefix, 0, 1) == "/"
  #  error_message = "The common_ssm_prefix value must be a valid SSM param path starting with '/'."
  #}
}
variable "ssm_kms_key_arn" {
  type        = string
  description = "the arn of the kms key used to read the common ssm params"

  #validation {
  #  condition     = length(var.ssm_kms_key_arn) > 4 && substr(var.ssm_kms_key_arn, 0, 4) == "arn:"
  #  error_message = "The ssm_kms_key_arn value must be a valid ARN id."
  #}
}

variable "vpc_id" {
  type        = string
  description = "the vpc id this asset is being deployed into"
}

variable "jitsi_vpc_cidr_blocks" {
  type        = list
  description = "list of cidr blocks that jitsi servers are deployed in, used in security group rules"
}

variable "jitsi_master_region" {
  type        = string
  description = "AWS Region the jitsi master lives in"
}

variable "ami" {
  type        = string
  description = "AMI id for the instance"
}

variable "instance_type" {
  type        = string
  default     = "t3.nano"
  description = "ec2 instance type"
}

variable "subnet_id" {
  description = "the subnet id to place the instance on"
  type        = string
}

variable "playbook_bucket" {
  description = "the s3 bucket id that the instance can read ansible playbooks from"
  type        = string
}

variable "playbook_bundle_s3_key" {
  description = "the path to the ansible bundle zip file in the s3 bucket"
  type        = string
}

variable "ssm_logs_bucket" {
  type        = string
  description = "S3 bucket name of the bucket where SSM Session Manager logs are stored"
}

variable "disable_api_termination" {
  type        = bool
  default     = true
  description = "Set to false to enable terraform destroy/re-apply"
}

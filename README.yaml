---
#
# This is the canonical configuration for the `README.md`
# Run `make readme` to rebuild the `README.md`
#

# Name of this project
name: jitsi-aws-deployment

# License of this project
license: "AGPL3"

# Canonical GitLab repo
gitlab_repo: guardianproject-ops/jitsi-aws-deployment

# Badges to display
badges: []

# Short description of this project
description: |-
  This is a starter package of infrastructure automation to deploy your own
  Jitsi Meet instance with SFUs in multiple geographic regions.

introduction: |-
  This package contains the building blocks you need to deploy to AWS a Jitsi
  Meet instance that features:

  * privacy friendly settings enabled by default
  * no tracking or third-party analytics
  * a single signaling instance (jicofo) in a region of your choice
  * multiple video bridges in various regions around the world
  * webserver geoip config to route your users to the closest videobridges to them
  * simple branding of the Jitsi Meet client
  * optional "secure domain" to limit meeting creation to authorized users

  It also can deploy a secure prometheus, alertmanager, grafana stack that is
  secured behind Cloudflare's Access and Argo Tunnel products.

  The grafana stack comes with dashboards to help you visualize the usage and
  health of your instance.

requirements: |-
  * You're familiar with terraform, ansible, packer, and AWS
  * You have the suite of CLI tools installed.  You can use [guardianproject-ops/packages](https://gitlab.com/guardianproject-ops/packages) to install them.
    * make
    * [aws2wrap](https://github.com/linaro-its/aws2-wrap)
    * [mozilla/sops](https://github.com/mozilla/sops)
    * aws-cli v2
    * terraform
    * ansible
    * jq
    * python3
  * You have an AWS account already provisioned and ready to go.
  * If you want to use the monitoring stack then you have:
    * A cloudflare account with Argo and Access enabled
    * Cloudflare api credentials
    * A PagerDuty/OpsGenie or other supported alert manager receiver (you'll need to configure this yourself)

# How to use this project
usage: |-

 This repo is designed to deploy and maintain multiple independent jitsi meet
 deployments. A "deployment" is considered one self-contained system consisting of:

  * A signaling instance running jicofo, prosody and serving the jitsi meet we bapp
  * One or more jitso videobridges (JVB) in one or more AWS regions
  * Zero or more TURN/STUN servers
  * A DNS entry for your Jitsi FQDN (e.g., `meet.example.com`)


  Each deployment should create a root directory in this repo, the example
  `meet.keanu.im` has been provided as a working template.

  To create a new deployment, simply copy the template, giving it a new name,
  and then perform a reset.

  ```
  cd meet.yourdomain.com
  make reset
  ```

 Then you must edit the config in several locations. All paths are relative to
 `meet.yourdomain.com`.

   * `Makefile.env`
   * `terraform/_vars/secrets.sops.yml`
   * `packer/config.yml`
   * `terraform/vpc-satellites`
   * `terraform/vpc-peering`

  Due to limitations in terraform, we cannot fully parameterize the terraform
  modules necessary to deploy instances across multiple AWS regions using only
  the regions as input.

  You must edit `vpc-satellites` and `vpc-peering` to create your satellite
  VPCs and peer them together. A "satellite" VPC is a VPC that hosts only a
  coturn or jitsi videobridge.

  Once all the configs are edited, and the CLI tools installed and configured,
  you can start spinning up the infrastructure. 

  ```
  make apply-all # WARNING: this will automatically spin up AWS infrastructure that will COST YOU MONEY
  ```

  To specific terraform roots, enter the `terraform` dir and `make <root module name>`, for example to apply the vpc-main config:

  ```
  cd terraform
  make vpc-main
  ```

  The various roots are depenent on eachother, look at the `apply-all` target
  of `terraform/Makefile` to see the required application order.

  If you do not use cloudflare as your DNS provider, you'll need to edit
  `terraform/jitsi-master/dns.tf` to correctly create an `A` record. You'll
  also need to edit `terraform/letsencrypt-certs/main.tf` and configure
  a [supported dns provider](https://www.terraform.io/docs/providers/acme/dns_providers/index.html).


related:
  - name: ansible-jitsi
    description: The ansible role responsible for configuring the instances
    url: https://gitlab.com/guardianproject-ops/ansible-jitsi
  - name: Jitsi HA Multi-region Operations Tips n Tricks
    description: A grab bag of knowledge
    url: https://gitlab.com/snippets/1964410

# Contributors to this project
contributors:
  - name: "Abel Luck"
    gitlab: "abelxluck"
    gravatar: 0f605397e0ead93a68e1be26dc26481a
